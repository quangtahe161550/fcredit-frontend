export const URIS = {
    DASHBOARD: "/dashboard",
    LOGIN: "/login",
    REGISTER: "/register",
    VERIFY_USER: "/verify-user",
    HOME: "/",
    DEBT: "/debt"

};